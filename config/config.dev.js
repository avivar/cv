const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpackDevServer = require('webpack-dev-server');
const { createCompiler, prepareUrls } = require('react-dev-utils/WebpackDevServerUtils');
const clearConsole = require('react-dev-utils/clearConsole');
const { join } = require('path');
const serverConfig = require('./config.dev.server');
const { babelRule, fileLoaderRule, miniCssRule, MiniCssExtractPlugin, appName } = require('./config.common');
const DEVELOPMENT = 'development';
const port = 5555;
const host = '0.0.0.0';

const htmlPlugin = new HtmlWebpackPlugin({
    template: join(process.cwd(), 'src', 'index.html'),
    filename: 'index.html'
})

const environmentVariablePlugin = new webpack.DefinePlugin({
    'ENVIRONMENT': JSON.stringify(DEVELOPMENT)
})

const compiler = createCompiler({
    webpack,
    appName,
    urls: prepareUrls('http', host, port),
    devtool: 'source-map',
    config: {
        mode: DEVELOPMENT,
        entry: join(process.cwd(), 'src', 'index.js'),
        output: {
            path: path.resolve('dist'),
            publicPath: '/',
            filename: 'bundle.js'
        },
        module: {
            rules: [
                babelRule,
                miniCssRule,
                fileLoaderRule
            ]
        },
        plugins: [
            environmentVariablePlugin,
            new webpack.NamedModulesPlugin,
            new webpack.HotModuleReplacementPlugin,
            htmlPlugin,
            new MiniCssExtractPlugin({
                filename: '[name].css',
                chunkFilename: '[id].css',
            })
        ]
    }
})

const server = new webpackDevServer(compiler, serverConfig);
server.listen(port, host, err => {
    clearConsole();
    console.log(err || `Starting development server at ${host}:${port}`);
})

process.on('unhandledRejection', (err)=>{
    console.log(`Error while starting dev server; Full stack trace:***`);
    console.log(err);
})