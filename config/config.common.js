const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const babelRule = {
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
        loader: 'babel-loader'
    }
}

const fileLoaderRule = {
    test: /\.(png|jpe?g|gif)$/i,
    use: [
      {
        loader: 'file-loader',
        options: {
          name: 'media/[name].[ext]'
        }
      },
    ],
}

const miniCssRule = {
    test: /\.css$/i,
    use: [
      {
        loader: MiniCssExtractPlugin.loader,
        options: {
          esModule: true,
        },
      },
      'css-loader',
    ],
}

module.exports.babelRule = babelRule;
module.exports.miniCssRule = miniCssRule;
module.exports.fileLoaderRule = fileLoaderRule;
module.exports.MiniCssExtractPlugin = MiniCssExtractPlugin;
module.exports.appName = "ReactStarter";