import React from "react"
import './aboutMe.css'
import cvData from '../../common/cvData'

const AboutMe = () => {
    let { contact } = cvData;
    return <div className='section-about-me'>
        <h1>{contact.fullName}</h1>
        <p>{contact.title}</p>
    </div>
}

export default AboutMe