import React from "react"
import './skills.css'
import LottieAnimation from '../LottieAnimation'
import wavesAnimation from '../../static/2677-ondas.json';
import Badge from '../Badge';
import cvData from '../../common/cvData';

const SkillContent = (props) => {
    return <React.Fragment>
        <div className='skill'>
            <div className='skill-content'>
                <span>{props.name}</span>
            </div>
            <div className='skill-level' style={{ height: `calc(70% - ${props.level}px)` }}>
                <LottieAnimation
                animation={wavesAnimation}
                width={58}
                height={40}
                style={
                    {
                        position: 'relative',
                        bottom: '40px'
                    }
                }/>
            </div>
        </div>
    </React.Fragment>
}

const Skills = () => {
    let { skills } = cvData;
    return <div className='skills-wrapper box'>
        <div className='skills-container'>
            <Badge name={'Skills'}/>
            {skills.map((skill) => {
                return <SkillContent level={skill.level * 10} name={skill.name}/>
            })}
        </div>
    </div>
}

export default Skills