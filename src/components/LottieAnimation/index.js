import React from "react"
import Lottie from 'react-lottie'

const LottieAnimation = (props) => {
    const lottieDefaultOptions = {
        loop: true,
        autoplay: true,
        animationData: props.animation,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    return <Lottie options={lottieDefaultOptions}
    height={props.height}
    width={props.width}
    style={props.style}
    />
}

export default LottieAnimation