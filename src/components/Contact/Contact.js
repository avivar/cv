import React from "react"
import './contact.css'
import { MobileTwoTone, MailTwoTone, HomeTwoTone } from '@ant-design/icons';
import cvData from '../../common/cvData'

const ContactInfoItem = (props) => {
    return <div className='contact-info-item'>
        {props.icon}
        <span className='contant-info-text'>{props.text}</span>
    </div>
}

const Contact = () => {
    let { contact } = cvData;
    return <React.Fragment>
        <ContactInfoItem icon={<MobileTwoTone className={'contact-info-icon'}/>} text={contact.phone}/>
        <ContactInfoItem icon={<MailTwoTone className={'contact-info-icon'}/>} text={contact.email}/>
        <ContactInfoItem icon={<HomeTwoTone className={'contact-info-icon'}/>} text={contact.address}/>
    </React.Fragment>
}

export default Contact