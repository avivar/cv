import React from "react"
import './badge.css'

const Badge = (props) => {
    return <div className='section-badge'>
        <span>{props.name}</span>
    </div>
}

export default Badge