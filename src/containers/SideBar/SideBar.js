import React from "react"
import Contact from '../../components/Contact'
import AboutMe from '../../components/AboutMe'

const SideBar = () => {
    return <aside className='side-bar-wrapper fx-col'>
            <nav className='fx-col fx-grow'>
              <div className='app-logo'>
                <a href='#/'><img src=''/></a>
              </div>
              <div id='appSideBar'>
                <AboutMe/>
                <Contact/>
              </div>
            </nav>
    </aside>
}

export default SideBar