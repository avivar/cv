import React, { Component } from "react"
import SideBar from "../SideBar"
import 'antd/dist/antd.css'
import Skills from '../../components/Skills'
import Education from '../../components/Education'

class App extends Component {
  render() {
    let modules = this.props.modules;
    return <div className='app'>
      <div className='fx-row'>
        <SideBar/>
        <div className='main fx-col'>
          <Skills/>
          <Education/>
        </div>
      </div>
    </div>
  }
}

export default App