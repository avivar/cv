export default {
    contact: {
        fullName: 'Aviv Arbitman',
        title: 'Web Developer',
        phone: '0543999656',
        address: 'Shnuel Hanagid 2, Tel Aviv',
        email: 'avivar1609@gmail.com'
    },
    skills: [
        {
            name: 'React',
            level: 3
        },
        {
            name: 'Node.js',
            level: 3
        },
        {
            name: 'Python',
            level: 3
        },
        {
            name: 'AWS',
            level: 3
        },
        {
            name: 'Html5',
            level: 3
        },
        {
            name: 'Docker',
            level: 3
        },
        {
            name: 'Webpack',
            level: 3
        },
        {
            name: 'PostgreSQL',
            level: 3
        },
        {
            name: 'Linux',
            level: 3
        }
    ]
}
